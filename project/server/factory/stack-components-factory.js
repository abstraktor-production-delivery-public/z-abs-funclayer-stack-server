
'use strict';


const StackComponentsCerts = require('./stack-components-certs');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const Fs = require('fs');
const Path = require('path');


class StackComponentsFactory {
  constructor() {
    this.loaded = false;
    this.stacksQueue = [];
    this.findStacks((err) => {
      if(err) {
        ddb.error('Error loading StackComponentsFactory', err);
      }
      this.registerStacks();
      this.loaded = true;
      while(0 !== this.stacksQueue.length) {
        this.stacksQueue.shift()(StackComponentsFactory._.stacks);
      }
    });
  }
  
  initExecution(executionId, debugDashboard) {
    StackComponentsFactory._.stacks.forEach((stack) => {
      if(stack.client.initExecution) {
        stack.client.initExecution(executionId, debugDashboard);
      }
      if(stack.server.initExecution) {
        stack.server.initExecution(executionId, debugDashboard);
      }
    });
  }
  
  exitExecution(executionId) {
    StackComponentsFactory._.stacks.forEach((stack) => {
      if(stack.client.exitExecution) {
        stack.client.exitExecution(executionId);
      }
      if(stack.server.exitExecution) {
        stack.server.exitExecution(executionId);
      }
    });
  }
  
  getStacks(done) {
    this.stacksQueue.push(done);
    if(this.loaded) {
      while(0 !== this.stacksQueue.length) {
        this.stacksQueue.shift()(StackComponentsFactory._.stacks);
      }
    }
  }
  
  createClient(stackName) {
    const stack = StackComponentsFactory._.stacks.get(stackName);
    if(undefined !== stack) {
      return stack.client;
    }
    else {
      throw(new Error(`Client stack: '${stackName}' does not exist.`));
    }
  }
  
  createServer(stackName) {
    const stack = StackComponentsFactory._.stacks.get(stackName);
    if(undefined !== stack) {
      return stack.server;
    }
    else {
      throw(new Error(`Server stack: '${stackName}' does not exist.`));
    }
  }
  
  supportInterceting(stackName, type, testData) {
    if('client' === type) {
      const client = this.createClient(stackName);
      if(client) {
        return client.supportInterception(testData);
      }
      return false;
    }
  }
  
  createTemplatesActors(stackName) {
    const stack = StackComponentsFactory._.stacks.get(stackName);
    if(undefined !== stack) {
      return stack.templatesActors;
    }
    else {
      throw(new Error(`Actor Template: '${stackName}' does not exist.`));
    }
  }
  
  createTemplatesTestCases(stackName) {
    const stack = StackComponentsFactory._.stacks.get(stackName);
    if(undefined !== stack) {
      return stack.templatesTestCases;
    }
    else {
      throw(new Error(`Test Case Template: '${stackName}' does not exist.`));
    }
  }
  
  registerStacks() {
    if(!abstractorIsApiAndStacksRegistered()) {
      const paths = [ActorPathDist.actorDistActorApiPath, ActorPathDist.actorDistNodejsApiPath, ActorPathDist.actorDistStackApiPath];
      StackComponentsFactory._.stacks.forEach((stack) => {
        paths.push(stack.path);
      });
      abstractorRegisterApiAndStacks(paths);
    }
    if(1 <= StackComponentsFactory._.stacks.size) {
      StackComponentsFactory._.stacks.forEach((stack) => {
        try {
          stack.client = require(`${stack.path}${Path.sep}${stack.name}-connection-client`);
          if(stack.client.initStack) {
            StackComponentsCerts.isLoaded((err) => {
              stack.client.initStack();
            });
          }
        }
        catch(err) {
          ddb.error(`Could not create Client for stack '` + stack.name + `'.`, err);
        }
        try {
          stack.server = require(`${stack.path}${Path.sep}${stack.name}-connection-server`);
        }
        catch(err) {
          //ddb.error(`Could not create Server for stack '` + stack.name + `'.`, err);
        }
        try {
          stack.templatesActors = require(`${stack.path}${Path.sep}${stack.name}-templates-actors`);
        }
        catch(err) {
          /*if('actorjs' === stack.name || 'http' === stack.name || 'icap' === stack.name || 'puppeteer' === stack.name || 'socket' === stack.name || 'websocket' === stack.name) { // TODO: remove this.
            ddb.error(`Could not create Actor templates for stack '` + stack.name + `'.`, err);
          }*/
        }
        try {
          stack.templatesTestCases = require(`${stack.path}${Path.sep}${stack.name}-templates-test-cases`);
        }
        catch(err) {
          /*if('actorjs' === stack.name || 'http' === stack.name || 'icap' === stack.name || 'puppeteer' === stack.name || 'socket' === stack.name || 'websocket' === stack.name) { // TODO: remove this.
            ddb.error(`Could not create Test Case templates for stack '` + stack.name + `'.`, err);
          }*/
        }
      });
    }
  }
  
  sortStacks() {
    StackComponentsFactory._.stacksList.sort((a, b) => {
      if(a[0] > b[0]) {
        return 1;
      }
      else if(a[0] < b[0]) {
        return -1;
      }
      else {
        return 0;
      }
    });
    StackComponentsFactory._.stacks = new Map(StackComponentsFactory._.stacksList);
  }
  
  findStacks(done) {
    let pendings = 2;
    let isError = false;
    this._findStacks(ActorPathDist.getActorDistStacksGlobalPath(), (err) => {
      if(!isError) {
        if(err) {
          isError = true;
          return done(err);
        }
        else if(0 === --pendings) {
          this.sortStacks();
          return done();
        }
      }
    });
    this._findStacks(ActorPathDist.getActorDistStacksLocalPath(), (err) => {
       if(!isError) {
        if(err && 'ENOENT' !== err.code) {
          isError = true;
          return done(err);
        }
        else if(0 === --pendings) {
          this.sortStacks();
          return done();
        }
      } 
    });
  }
  
  _findStacks(loadPath, done) {
    Fs.readdir(loadPath, (err, files) => {
      if(err) {
        return done(err);
      }
      if(undefined === files || 0 === files.length) {
        return done();
      }
      let firstError;
      let pendings = files.length;
      files.forEach((dir) => {
        const path = `${loadPath}${Path.sep}${dir}`;
        Fs.lstat(path, (err, stat) => {
          if(undefined === firstError) {
            if(stat.isDirectory()) {
              try {
                StackComponentsFactory._.stacksList.push([dir, {
                  name: dir,
                  path: path,
                  client: '',
                  server: '',
                  templatesActors: null,
                  templatesTestCases: null
                }]);
              }
              catch(errCatch) {
                done(errCatch);
                // TODO: addlog error here.
              }
            }
            if(0 === --pendings) {
              done();
            }
          }
        });
      });
    });
  }
}

StackComponentsFactory._ = {
  stacks: new Map(),
  stacksList: []
};


module.exports = new StackComponentsFactory();
