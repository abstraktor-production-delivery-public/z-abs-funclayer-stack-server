
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');
const Path = require('path');


class StackComponentsCerts {
  constructor() {
    this.cert = null;
    this.key = null;
    this.ca = null;
    this.queue = [];
    this.loaded = false;
    this.loadCerts(() => {
      this.loaded = true;
      while(0 !== this.queue.length) {
        this.queue.shift()();
      }
    });
  }
  
  isLoaded(done) {
    this.queue.push(done);
    if(this.loaded) {
      while(0 !== this.queue.length) {
        this.queue.shift()();
      }
    }
  }
  
  loadCerts(done) {
    let pendings = 3;
    Fs.readFile(`${ActorPathData.getCertsGlobalFolder()}${Path.sep}server.crt`, (err, cert) => {
      if(err) {
        ddb.error(err);
      }
      this.cert = cert;
      if(0 === --pendings) {
        done();
      }
    });
    Fs.readFile(`${ActorPathData.getCertsGlobalFolder()}${Path.sep}server.key`, (err, key) => {
      if(err) {
        ddb.error(err);
      }
      this.key = key;
      if(0 === --pendings) {
        done();
      }
    });
    Fs.readFile(`${ActorPathData.getCertsGlobalFolder()}${Path.sep}server.crt`, (err, ca) => {
      if(err) {
        ddb.error(err);
      }
      this.ca = ca;
      if(0 === --pendings) {
        done();
      }
    });
  }
}


module.exports = new StackComponentsCerts();
